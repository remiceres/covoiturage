/**
 * Fichier triggers.sql
 * Contient les définitions de procédures et triggers
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */

CREATE DATABASE IF NOT EXISTS covoiturage
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE covoiturage;
DELIMITER $$

-- Procédure User_check
--
-- Vérifie l’intégrité d’une modification de tuple dans
-- la table User
--
-- Paramètres : valeurs du tuple
-- Signaux : lève un 4502* en cas de tuple invalide

CREATE PROCEDURE User_check(
    IN email VARCHAR(256),
    IN role VARCHAR(5),
    IN birthdate DATE,
    IN tel_indic VARCHAR(3),
    IN tel_number VARCHAR(16)
)
BEGIN
    IF email NOT LIKE '%@%.%' THEN
        SIGNAL SQLSTATE '45021'
        SET MESSAGE_TEXT = 'Format de l''adresse email invalide';
    END IF;

    IF role NOT IN ('admin', 'user') THEN
        SIGNAL SQLSTATE '45022'
        SET MESSAGE_TEXT = 'Le rôle de l’utilisateur doit être ''admin'' ou ''user''';
    END IF;

    IF DATEDIFF(CURRENT_DATE(), birthdate) <= 0 THEN
        SIGNAL SQLSTATE '45023'
        SET MESSAGE_TEXT = 'Félicitations, vous êtes né dans le futur';
    END IF;

    IF tel_indic NOT REGEXP '[0-9]{1,3}' THEN
        SIGNAL SQLSTATE '45024'
        SET MESSAGE_TEXT = 'Format de l’indicatif téléphonique invalide';
    END IF;

    IF tel_number NOT REGEXP '[0-9]{1,}' THEN
        SIGNAL SQLSTATE '45025'
        SET MESSAGE_TEXT = 'Format du numéro de téléphone invalide';
    END IF;
END$$

-- Procédure Trip_check
--
-- Vérifie l’intégrité d’une modification de tuple dans
-- la table Trip
--
-- Paramètres : valeurs du tuple
-- Signaux : lève un 4503* en cas de tuple invalide

CREATE PROCEDURE Trip_check(
    IN price NUMERIC(6),
    IN seats NUMERIC(3),
    IN drive VARCHAR(256),
    IN use_vehicle VARCHAR(16)
)
BEGIN
    IF price <= 0 THEN
        SIGNAL SQLSTATE '45031'
        SET MESSAGE_TEXT = 'Le prix du voyage doit être strictement positif';
    END IF;

    IF seats <= 0 THEN
        SIGNAL SQLSTATE '45032'
        SET MESSAGE_TEXT = 'Le voyage doit proposer au moins une place libre';
    END IF;

    IF use_vehicle NOT IN (
        SELECT vehicle
          FROM VehicleOwn
         WHERE VehicleOwn.user = drive
    ) THEN
        SIGNAL SQLSTATE '45033'
        SET MESSAGE_TEXT = 'Ce véhicule n’appartient pas au conducteur';
    END IF;
END$$

-- Procédure Stop_check
--
-- Vérifie l’intégrité d’une modification de tuple dans
-- la table Stop
--
-- Paramètres : valeurs du tuple
-- Signaux : lève un 4504* en cas de tuple invalide

CREATE PROCEDURE Stop_check(
    IN id NUMERIC,
    IN meet_time DATETIME,
    IN trip NUMERIC
)
BEGIN
    IF meet_time IN (
        SELECT Stop.meet_time
          FROM Stop
         WHERE Stop.trip = trip
               AND Stop.id <> id
    ) THEN
        SIGNAL SQLSTATE '45043'
        SET MESSAGE_TEXT = 'Une autre étape est déjà prévue à cette heure sur ce trajet';
    END IF;

    -- Dans le cas où on ait changé de ville de départ ou d'arrivée,
    -- il faut vérifier le respect des contraintes
    CALL Trip_price_check(trip);
END$$

-- Procédure TripReview_check
--
-- Vérifie l’intégrité d’une modification de tuple dans
-- la table TripReview
--
-- Paramètres : valeurs du tuple
-- Signaux : lève un 4505* en cas de tuple invalide

CREATE PROCEDURE TripReview_check(
    IN from_user VARCHAR(256),
    IN about_user VARCHAR(256),
    IN trip NUMERIC,
    IN rating NUMERIC(1)
)
BEGIN
    DECLARE date_end DATETIME;

    IF rating NOT BETWEEN 0 AND 5 THEN
        SIGNAL SQLSTATE '45051'
        SET MESSAGE_TEXT = 'La notation doit être comprise entre 0 et 5 inclus';
    END IF;

    IF from_user = about_user THEN
        SIGNAL SQLSTATE '45052'
        SET MESSAGE_TEXT = 'Un utilisateur ne peut pas laisser d’avis sur lui-même';
    END IF;

    IF from_user NOT IN(
        SELECT TripParticipants.participant
        FROM TripParticipants
        WHERE TripParticipants.trip = trip
    )
    THEN
        SIGNAL SQLSTATE '45053'
        SET MESSAGE_TEXT = 'L''auteur du message ne fait pas partie du voyage';
    END IF;

    IF about_user NOT IN(
        SELECT TripParticipants.participant
        FROM TripParticipants
        WHERE TripParticipants.trip = trip
    )
    THEN
        SIGNAL SQLSTATE '45053'
        SET MESSAGE_TEXT = 'Le sujet du message ne fait pas partie du voyage';
    END IF;

    IF from_user IN (
            SELECT TripParticipants.participant
            FROM TripParticipants
            WHERE TripParticipants.type = 'driver'
    )
    THEN
        SELECT Stop.meet_time
        INTO date_end
        FROM Trip, Stop
        WHERE Trip.id = Stop.trip
            AND Trip.id = trip
        ORDER BY Stop.meet_time DESC
        LIMIT 1;
    END IF;

    IF from_user IN (
            SELECT TripParticipants.participant
            FROM TripParticipants
            WHERE TripParticipants.type = 'participant'
    )
    THEN
        SELECT Stop.meet_time
        INTO date_end
        FROM Trip, Stop, StopDeposit
        WHERE Trip.id = Stop.trip
            AND Stop.id = StopDeposit.stop
            AND Trip.id = trip
            AND StopDeposit.email = from_user
        ORDER BY Stop.meet_time DESC
        LIMIT 1;
    END IF;

    IF date_end > NOW() THEN
        SIGNAL SQLSTATE '45054'
        SET MESSAGE_TEXT = 'on ne peu pas laisser d''avis avant la fin du voyage';
    END IF;
END$$

-- Procédure CityLinkData_check
--
-- Vérifie l’intégrité d’une modification de tuple dans
-- la table CityLinkData
--
-- Paramètres : valeurs du tuple
-- Signaux : lève un 4506* en cas de tuple invalide

CREATE PROCEDURE CityLinkData_check(
    IN max_price NUMERIC(6)
)
BEGIN
    IF max_price <= 0 THEN
        SIGNAL SQLSTATE '45061'
        SET MESSAGE_TEXT = 'Le seuil de prix ne peut pas être négatif';
    END IF;
END$$

-- Procédure Trip_add_passenger
--
-- Ajoute un passager dans un trajet, qui monte à un arrêt
-- donné et descend à un autre.
--
-- Paramètres :
--     - email (identifiant du passager)
--     - start (arrêt de montée dans le véhicule)
--     - end (arrêt de descente du véhicule)
-- Signaux :
--     - 45071 (l’ordre de montée et de descente est incorrect)
--     - 45072 (l’arrêt de descente n’est pas dans le même voyage que le départ)
--     - 45073 (le trajet a été annulé)
--     - 45074 (le conducteur ne peut pas être passager du trajet)
--     - 45075 (toutes les places sont prises sur la section demandée)
--     - 45081 (toutes les places disponibles sont occupées)
--     - 45082 (le passager est déjà dans le véhicule au moment de la montée)

CREATE PROCEDURE Trip_add_passenger(
    email VARCHAR(256),
    start NUMERIC,
    end NUMERIC
)
BEGIN
    -- Trajets de l’arrêt de départ et d’arrivée
    -- (normalement, ce sont les mêmes)
    DECLARE trip_start NUMERIC;
    DECLARE trip_end NUMERIC;

    -- Heure de départ et d’arrivée du trajet
    DECLARE start_time DATETIME;
    DECLARE end_time DATETIME;

    -- Valeurs du trajet correspondant
    DECLARE seats NUMERIC;
    DECLARE valid INT;
    DECLARE drive VARCHAR(256);

    -- Boucle sur les arrêts intermédiaires
    DECLARE stop NUMERIC;
    DECLARE loop_done INT DEFAULT FALSE;

    DECLARE intermediate_stops CURSOR FOR
        SELECT Stop.id
          FROM Stop
         WHERE Stop.meet_time >= start_time
               AND Stop.meet_time < end_time
               AND Stop.trip = trip_start;

    -- Annulation de la transaction en cas d’erreur
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        RESIGNAL;
    END;

    DECLARE CONTINUE HANDLER FOR NOT FOUND
    BEGIN
        SET loop_done = TRUE;
    END;

    START TRANSACTION;

    SELECT Stop.trip, Stop.meet_time
      INTO trip_start, start_time
      FROM Stop
     WHERE Stop.id = start;

    SELECT Stop.trip, Stop.meet_time
      INTO trip_end, end_time
      FROM Stop
     WHERE Stop.id = end;

    IF trip_start <> trip_end THEN
        SIGNAL SQLSTATE '45072'
        SET MESSAGE_TEXT = 'Le départ et l''arrivée ne font pas partie du même trajet';
    END IF;

    SELECT Trip.seats, Trip.valid, Trip.drive
      INTO seats, valid, drive
      FROM Trip
     WHERE Trip.id = trip_start;

    IF NOT valid THEN
        SIGNAL SQLSTATE '45073'
        SET MESSAGE_TEXT = 'Le trajet a été annulé';
    END IF;

    IF drive = email THEN
        SIGNAL SQLSTATE '45074'
        SET MESSAGE_TEXT = 'Le conducteur ne peut pas être son propre passager';
    END IF;

    -- Vérifie qu’à chaque arrêt intermédiaire du trajet, il y a assez de place
    OPEN intermediate_stops;

    stop_loop: LOOP
        FETCH intermediate_stops INTO stop;

        IF loop_done THEN
            LEAVE stop_loop;
        END IF;

        IF get_occupied_seats(stop) >= seats THEN
            SIGNAL SQLSTATE '45075'
            SET MESSAGE_TEXT = 'Plus de place disponible pour ce trajet';
        END IF;
    END LOOP;

    CLOSE intermediate_stops;

    INSERT INTO StopPickup VALUES(email, start);
    INSERT INTO StopDeposit VALUES(email, end);

    COMMIT;
END$$

-- Procédure StopDeposit_check
--
-- Vérifie l'intégrité d'une modification de tuble dans
-- la table StopDeposit
--
-- Paramètres : valeurs du tuple
-- Signaux : lève une 4507* en cas de tuple invalide

CREATE PROCEDURE StopDeposit_check(
    IN email VARCHAR(256),
    IN stop NUMERIC
)
BEGIN
    DECLARE meet_time DATETIME;
    DECLARE trip NUMERIC;

    SELECT Stop.meet_time, Stop.trip
      INTO meet_time, trip
      FROM Stop
     WHERE Stop.id = stop;

    -- Vérifie que l'utilisateur fasse bien partie des passagers
    -- présents dans le véhicule à cette étape
    IF email NOT IN (
        SELECT StopPickup.email
          FROM Stop Stop1, StopPickup
         WHERE Stop1.id = StopPickup.stop
               AND Stop1.trip = trip
               AND Stop1.meet_time < meet_time
               AND StopPickup.email NOT IN (
                   -- On ne prend pas en compte ceux qui ont déjà été
                   -- déposés à une étape précédente
                   SELECT StopDeposit.email
                     FROM Stop Stop2, StopDeposit
                    WHERE Stop2.id = StopDeposit.stop
                          AND Stop2.trip = trip
                          AND Stop2.meet_time > Stop1.meet_time
                          AND Stop2.meet_time < meet_time
               )
    ) THEN
        SIGNAL SQLSTATE '45071'
        SET MESSAGE_TEXT = 'Un passager qui n''est pas dans le véhicule ne peut pas être déposé';
    END IF;
END$$

-- Procédure StopPickup_check
--
-- Vérifie l'intégrité d'une modification de tuples dans
-- la table StopPickup
--
-- Paramères : valeurs du tuples
-- Signeaux : lève une 4508* en cas de tuple invalide

CREATE PROCEDURE StopPickup_check(
    IN email VARCHAR(256),
    IN stop NUMERIC
)
BEGIN
    DECLARE meet_time DATETIME;
    DECLARE trip NUMERIC;

    SELECT Stop.meet_time, Stop.trip
      INTO meet_time, trip
      FROM Trip, Stop
     WHERE Stop.trip = Trip.id
           AND Stop.id = stop;

    -- Vérifie que l'utilisateur à récupérer ne fasse pas partie des passagers
    -- présents dans le véhicule à cette étape
    IF email IN (
        SELECT StopPickup.email
          FROM Stop Stop1, StopPickup
         WHERE Stop1.id = StopPickup.stop
               AND Stop1.trip = trip
               AND Stop1.meet_time < meet_time
               AND StopPickup.email NOT IN (
                   -- On ne prend pas en compte ceux qui ont déjà été
                   -- déposés à une étape précédente
                   SELECT StopDeposit.email
                     FROM Stop Stop2, StopDeposit
                    WHERE Stop2.id = StopDeposit.stop
                          AND Stop2.trip = trip
                          AND Stop2.meet_time > Stop1.meet_time
                          AND Stop2.meet_time < meet_time
               )
    ) THEN
        SIGNAL SQLSTATE '45082'
        SET MESSAGE_TEXT = 'Un passager qui est déjà dans le véhicule ne peut pas être récupéré';
    END IF;
END$$

-- Procédure Trip_price_check
--
-- Vérifie qu'un trajet satisfasse les limites de prix établies
-- dans la table CityLink entre sa ville de départ et sa ville d'arrivée
--
-- Paramètres :
--     - trip (identifiant du trajet à contrôler)
-- Erreurs :
--     - 45091 (le trajet ne respecte pas les limites établies)

CREATE PROCEDURE Trip_price_check(IN trip NUMERIC)
BEGIN
    DECLARE start_city NUMERIC;
    DECLARE end_city NUMERIC;
    DECLARE max_price NUMERIC;

      SELECT City.id INTO start_city
        FROM Stop, Address, City
       WHERE Stop.be_at = Address.id
             AND Address.be_in = City.id
             AND Stop.trip = trip
    ORDER BY Stop.meet_time ASC
       LIMIT 1;

      SELECT City.id INTO end_city
        FROM Stop, Address, City
       WHERE Stop.be_at = Address.id
             AND Address.be_in = City.id
             AND Stop.trip = trip
    ORDER BY Stop.meet_time DESC
       LIMIT 1;

    SELECT CityLink.max_price INTO max_price
      FROM CityLink
     WHERE CityLink.from_city = start_city
           AND CityLink.to_city = end_city;

    IF (
        SELECT Trip.price
          FROM Trip
         WHERE Trip.id = 1
    ) > max_price THEN
        SIGNAL SQLSTATE '45091'
        SET MESSAGE_TEXT = 'Le trajet ne respecte pas la limite de prix établie entre sa ville de départ et sa ville d''arrivée';
    END IF;
END$$

-- Triggers

CREATE TRIGGER User_before_insert
BEFORE INSERT ON User
FOR EACH ROW
BEGIN
    CALL User_check(
        new.email, new.role, new.birthdate,
        new.tel_indic, new.tel_number
    );
END$$

CREATE TRIGGER User_before_update
BEFORE UPDATE ON User
FOR EACH ROW
BEGIN
    CALL User_check(
        new.email, new.role, new.birthdate,
        new.tel_indic, new.tel_number
    );
END$$

CREATE TRIGGER Trip_before_insert
BEFORE INSERT ON Trip
FOR EACH ROW
BEGIN
    CALL Trip_check(new.price, new.seats, new.drive, new.use_vehicle);
END$$

CREATE TRIGGER Trip_before_update
BEFORE UPDATE ON Trip
FOR EACH ROW
BEGIN
    DECLARE stop NUMERIC;
    DECLARE loop_done INT DEFAULT FALSE;
    DECLARE trip_stops CURSOR FOR
        SELECT Stop.id
          FROM Stop
         WHERE Stop.trip = new.id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND
    BEGIN
        SET loop_done = TRUE;
    END;

    CALL Trip_check(new.price, new.seats, new.drive, new.use_vehicle);

    -- S’il existe des inscrits sur le trajet, il n’est
    -- pas possible de changer le prix
    IF new.price <> old.price AND EXISTS (
        SELECT *
          FROM Stop, StopPickup
         WHERE Stop.trip = new.id
               AND StopPickup.stop = Stop.id
        UNION
        SELECT *
          FROM Stop, StopDeposit
         WHERE Stop.trip = new.id
               AND StopDeposit.stop = Stop.id
    ) THEN
        SIGNAL SQLSTATE '45034'
        SET MESSAGE_TEXT = 'Impossible de modifier le prix du trajet car il y a déjà des passagers inscrits';
    END IF;

    -- Si on change le prix, il faut rester dans les limites
    -- définies éventuellement par CityLink
    IF new.price <> old.price THEN
        CALL Trip_price_check(new.id);
    END IF;

    -- Si on a réduit le nombre de places dans le trajet
    -- il faut vérifier qu’elles ne sont pas déjà occupées
    IF new.seats < old.seats THEN
        OPEN trip_stops;

        stop_loop: LOOP
            FETCH trip_stops INTO stop;

            IF loop_done THEN
                LEAVE stop_loop;
            END IF;

            IF get_occupied_seats(stop) > new.seats THEN
                SIGNAL SQLSTATE '45036'
                SET MESSAGE_TEXT = 'Impossible de réduire le nombre de places car il y a déjà des utilisateurs inscrits sur ce trajet';
            END IF;
        END LOOP;

        CLOSE trip_stops;
    END IF;
END$$

CREATE TRIGGER Stop_before_insert
BEFORE INSERT ON Stop
FOR EACH ROW
BEGIN
    CALL Stop_check(new.id, new.meet_time, new.trip);
END$$

CREATE TRIGGER Stop_before_update
BEFORE UPDATE ON Stop
FOR EACH ROW
BEGIN
    CALL Stop_check(new.id, new.meet_time, new.trip);

    -- S’il existe déjà des inscrits pour cet arrêt, on empêche toute
    -- modification
    IF EXISTS (
        SELECT *
          FROM StopDeposit
         WHERE StopDeposit.stop = new.id
        UNION
        SELECT *
          FROM StopPickup
         WHERE StopPickup.stop = new.id
    ) THEN
        SIGNAL SQLSTATE '45044'
        SET MESSAGE_TEXT = 'Impossible d’éditer une étape sur laquelle est inscrit un passager';
    END IF;
END$$

CREATE TRIGGER Stop_before_delete
BEFORE DELETE ON Stop
FOR EACH ROW
BEGIN
    -- Dans le cas où on ait changé de ville de départ ou d'arrivée,
    -- il faut vérifier le respect des contraintes
    CALL Trip_price_check(trip);
END$$

CREATE TRIGGER TripReview_before_insert
BEFORE INSERT ON TripReview
FOR EACH ROW
BEGIN
    CALL TripReview_check(new.from_user, new.about_user, new.trip, new.rating);
END$$

CREATE TRIGGER TripReview_before_update
BEFORE UPDATE ON TripReview
FOR EACH ROW
BEGIN
    CALL TripReview_check(new.from_user, new.about_user, new.trip, new.rating);
END$$

CREATE TRIGGER CityLinkData_before_insert
BEFORE INSERT ON CityLinkData
FOR EACH ROW
BEGIN
    CALL CityLinkData_check(new.max_price);
END$$

CREATE TRIGGER CityLinkData_before_update
BEFORE UPDATE ON CityLinkData
FOR EACH ROW
BEGIN
    CALL CityLinkData_check(new.max_price);
END$$

CREATE TRIGGER StopDeposit_before_insert
BEFORE INSERT ON StopDeposit
FOR EACH ROW
BEGIN
    CALL StopDeposit_check(new.email, new.stop);
END$$

CREATE TRIGGER StopDeposit_before_update
BEFORE UPDATE ON StopDeposit
FOR EACH ROW
BEGIN
    CALL StopDeposit_check(new.email, new.stop);
END$$

CREATE TRIGGER StopDeposit_after_delete
AFTER DELETE ON StopDeposit
FOR EACH ROW
BEGIN
    DECLARE trip NUMERIC;
    DECLARE deposit_time DATETIME;
    DECLARE pick_stop NUMERIC;

    SELECT Stop.trip, Stop.meet_time
      INTO trip, deposit_time
      FROM Stop
     WHERE Stop.id = old.stop;

      SELECT Stop.id INTO pick_stop
        FROM Stop, StopPickup
       WHERE Stop.id = StopPickup.stop
             AND StopPickup.email = old.email
             AND Stop.trip = trip
             AND Stop.meet_time < deposit_time
    ORDER BY Stop.meet_time DESC
       LIMIT 1;

    IF pick_stop IS NOT NULL THEN
        DELETE FROM StopPickup
        WHERE StopPickup.stop = pick_stop
              AND StopPickup.email = old.email;
    END IF;
END$$

CREATE TRIGGER StopPickup_before_insert
BEFORE INSERT ON StopPickup
FOR EACH ROW
BEGIN
    CALL StopPickup_check(new.email, new.stop);
END$$

CREATE TRIGGER StopPickup_before_update
BEFORE UPDATE ON StopPickup
FOR EACH ROW
BEGIN
    CALL StopPickup_check(new.email, new.stop);
END$$

CREATE TRIGGER StopPickup_after_delete
AFTER DELETE ON StopPickup
FOR EACH ROW
BEGIN
    DECLARE trip NUMERIC;
    DECLARE pick_time DATETIME;
    DECLARE deposit_stop NUMERIC;

    SELECT Stop.trip, Stop.meet_time
      INTO trip, pick_time
      FROM Stop
     WHERE Stop.id = old.stop;

      SELECT Stop.id INTO deposit_stop
        FROM Stop, StopDeposit
       WHERE Stop.id = StopDeposit.stop
             AND StopDeposit.email = old.email
             AND Stop.trip = trip
             AND Stop.meet_time > pick_time
    ORDER BY Stop.meet_time ASC
       LIMIT 1;

    IF deposit_stop IS NOT NULL THEN
        DELETE FROM StopDeposit
        WHERE StopDeposit.stop = deposit_stop
              AND StopDeposit.email = old.email;
END IF;
END$$

DELIMITER ;
