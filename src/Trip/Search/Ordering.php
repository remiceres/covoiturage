<?php
namespace Trip\Search;

/**
 * Énumère les tris possibles pour les résultats d’une recherche
 * de trajet.
 */
class Ordering
{
    // Ordonne les résultats du plus proche des points spécifiés
    // au plus éloigné
    public const PROXIMITY = 0;

    // Ordonne les résultats du plus proche dans le temps au plus éloigné
    public const DATE = 1;

    // Ordonne les résultats du moins cher au plus cher
    public const PRICE = 2;

    // Ordonne les résultats de la meilleure note conducteur à la moins
    // bonne note conducteur
    public const RATING = 3;
}
