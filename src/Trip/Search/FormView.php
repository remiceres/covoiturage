<?php
namespace Trip\Search;

/**
 * Vue pour le formulaire de recherche de trajet.
 */
class FormView implements \IView
{
    private $search;
    private $form;

    public function __construct(SearchModel $search, \Form\Model $form)
    {
        $this->search = $search;
        $this->form = $form;
    }

    public function render()
    {
        $cities = $this->search->fetchCities();
        $order = '';

        if (count($this->search->getOrder()) >= 1)
        {
            $order = $this->search->getOrder()[0];
        }
?>
<form method="GET" class="solid">
<div class="columns">
    <datalist id="trip-search-form-cities">
        <?php
        foreach ($cities as $city):
        ?>
        <option value="<?= $city ?>">
        <?php
        endforeach;
        ?>
    </datalist>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'start-city');
    ?>
    <p>
        <label for="trip-search-form-start-city">Ville de départ :</label>
        <input type="text" list="trip-search-form-cities" required
            name="start-city" id="trip-search-form-start-city"
            value="<?= $this->search->getStartCity() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'end-city');
    ?>
    <p>
        <label for="trip-search-form-end-city">Ville d’arrivée :</label>
        <input type="text" list="trip-search-form-cities" required
            name="end-city" id="trip-search-form-end-city"
            value="<?= $this->search->getEndCity() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'start-time');
    ?>
    <p>
        <label for="trip-search-form-start-date">Date de départ :</label>
        <input type="date" name="start-date" id="trip-search-form-start-date"
            value="<?= ($this->search->getStartTime()
                ? $this->search->getStartTime() : new \DateTime())
                ->format(\Constants::DATE_ISO_FORMAT) ?>">
        <input type="time" name="start-time" id="trip-search-form-start-time"
            step="60" value="<?= ($this->search->getStartTime()
                ? $this->search->getStartTime() : new \DateTime())
                ->format(\Constants::SHORT_TIME_ISO_FORMAT) ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'end-time');
    ?>
    <p>
        <label for="trip-search-form-end-date">Date d’arrivée :</label>
        <input type="date" name="end-date" id="trip-search-form-end-date"
            value="<?= $this->search->getEndTime()
                ? $this->search->getEndTime()->format(
                    \Constants::DATE_ISO_FORMAT
                ) : '' ?>">
        <input type="time" name="end-time" id="trip-search-form-end-time"
            step="60" value="<?= $this->search->getEndTime()
                ? $this->search->getEndTime()->format(
                    \Constants::SHORT_TIME_ISO_FORMAT
                ) : '' ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'max-price');
    ?>
    <p>
        <label for="trip-search-form-max-price">Prix maximal :</label>
        <input type="number" min="0" step="0.01" name="max-price"
            id="trip-search-form-max-price"
            value="<?= $this->search->getMaxPrice() < PHP_INT_MAX
                ? number_format($this->search->getMaxPrice() / 100.0, 2, '.', '')
                : '' ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'min-rating');
    ?>
    <p>
        <label for="trip-search-form-min-rating">Note minimale :</label>
        <input type="number" min="0" step="1" max="5" name="min-rating"
            id="trip-search-form-min-rating"
            value="<?= $this->search->getMinRating() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'nb-places');
    ?>
    <p>
        <label for="trip-search-form-nb-places">Nombre de places :</label>
        <input type="number" min="1" step="1" max="10" name="nb-places"
            id="trip-search-form-nb-places"
            value="<?= $this->search->getNbPlaces() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'order');
    ?>
    <p>
        <label for="trip-search-form-order">Ordonner par :</label>
        <select name="order" id="trip-search-form-order">
            <option value="proximity"<?= $order === Ordering::PROXIMITY ? ' selected' : '' ?>>
                Proximité du lieu de départ et d’arrivée
            </option>
            <option value="date"<?= $order === Ordering::DATE ? ' selected' : '' ?>>
                Proximité de la date
            </option>
            <option value="price"<?= $order === Ordering::PRICE ? ' selected' : '' ?>>
                Prix le moins élevé
            </option>
            <option value="rating"<?= $order === Ordering::RATING ? ' selected' : '' ?>>
                Meilleure note conducteur
            </option>
        </select>
    </p>
</div>

<p>
    <input type="submit" value="Rechercher">
</p>
</form>
<?php
    }
}
