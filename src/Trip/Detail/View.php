<?php
namespace Trip\Detail;

/**
 * Vue pour les détails d'un trajet.
 */
class View implements \IView
{
    private $router;
    private $auth;
    private $model;
    private $form;

    public function __construct(
        \Router $router, \User\Authenticator $auth,
        Model $model, \Form\Model $form
    )
    {
        $this->router = $router;
        $this->auth = $auth;
        $this->model = $model;
        $this->form = $form;
    }

    public function render()
    {
        $trip = $this->model->fetchTripDetails();
        $stops = $this->model->fetchStopDetails();

        // Gestion des requêtes invalides
        if (!$trip || $trip->start_time > $trip->end_time || !$trip->valid)
        {
            $this->router->redirect('home');
        }
?>
<h2>Détails du trajet</h2>
<h3><?= $trip->start_city ?> — <?= $trip->end_city ?></h3>

<div class="solid">
    <dl>
        <dt>Conducteur</dt>
        <dd><?= $trip->driver_name ?></dd>
        <dt>Véhicule</dt>
        <dd><?= $trip->vehicle_brand ?> <?= $trip->vehicle_model ?> <?= $trip->vehicle_color ?></dd>
        <dt>Prix du trajet</dt>
        <dd><?= money_format('%.2n', $trip->total_price / 100) ?></dd>
        <dt>Distance du trajet</dt>
        <dd><?= number_format($trip->distance, 2, ',', ' ') ?> km</dd>
        <dt>Nombre de places disponibles</dt>
        <dd><?= $trip->available_seats ?></dd>
        <dt>Heure de départ</dt>
        <dd><?= strftime(\Constants::DATETIME_USER_FORMAT, $trip->start_time->getTimestamp())?></dd>
        <dt>Heure d’arrivée</dt>
        <dd><?= strftime(\Constants::DATETIME_USER_FORMAT, $trip->end_time->getTimestamp())?></dd>
    </dl>
</div>

<h3>Gestion de l’inscription</h3>

<form method="POST" class="solid">
    <?php \Form\ViewHelpers::renderErrors($this->form, 'general'); ?>

    <?php if (!$this->auth->hasRole(\User\Role::USER)): ?>
    <p>
        <em>Veuillez <a href="<?= $this->router->generate('userLogin') ?>">vous connecter</a> pour pouvoir gérer votre inscription à ce trajet.</em>
    </p>
    <?php elseif ($trip->start_time < new \DateTime()): ?>
    <p>
        <em>Ce trajet a déjà eu lieu, il n’est pas possible de <?= $trip->participates ? 's’en désinscrire' : 's’y inscrire' ?>.</em>
    </p>
    <?php elseif ($trip->driver_email === $this->auth->getEmail()): ?>
    <p>
        <em>Vous conduisez ce trajet.</em>
    </p>
    <?php elseif ($trip->participates): ?>
    <p>
        Vous êtes inscrit sur ce trajet.
    </p>
    <p>
        <input type="hidden" name="action" value="remove">
        <input type="submit" value="Se désinscrire">
    </p>
    <?php elseif ($trip->available_seats > 0): ?>
    <p>
        <input type="hidden" name="action" value="add">
        <input type="submit" value="S’inscrire">
    </p>
    <?php endif; ?>
</form>

<h3>Liste des arrêts</h3>

<table class="solid">
    <tr>
        <th>Ville</th>
        <th>Heure de passage</th>
        <th>Actions</th>
    </tr>
    <?php
    foreach ($stops as $stop)
    {
    ?>
        <tr>
            <td><?= $stop->city_name ?></td>
            <td><?= strftime(\Constants::DATETIME_USER_FORMAT, $stop->stop_time->getTimestamp())?></td>
            <td>
                <?php if ($stop->stop_time < $trip->end_time): ?>
                <a href="<?= $this->router->generate('tripShow', [
                    'trip' => $this->model->getTrip(),
                    'start' => $stop->stop_id,
                    'end' => $this->model->getEnd()
                ]) ?>">Partir d’ici</a>
                <?php endif; ?>

                <?php if ($stop->stop_time > $trip->start_time): ?>
                <a href="<?= $this->router->generate('tripShow', [
                    'trip' => $this->model->getTrip(),
                    'start' => $this->model->getStart(),
                    'end' => $stop->stop_id
                ]) ?>">Aller ici</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php
    }
    ?>
</table>
    <?php
    }
}
