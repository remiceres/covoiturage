<?php
namespace Trip\Detail;

/**
 * Modèle pour les détails d'un trajet.
 */
class Model
{
     // Référence à la connexion vers la base de données
     private $db;

     // Référence à l’authentificateur
     private $auth;

     // Identifiant du trajet
     private $trip;

     // Identifiant de l'étape de départ
     private $start;

     // Identifiant de l'étape d'arrivée
     private $end;

    /**
     * Construit le modèle de Trip/Detail
     *
     * @param db Référence à la connexion vers la base de données à utiliser
     * pour récupérer les données de ce modèle.
     * @param auth Authentificateur d’utilisateur.
     */
    public function __construct(\PDO $db, \User\Authenticator $auth)
    {
        $this->db = $db;
        $this->auth = $auth;
    }

    /**
     * Récupère les informations sur le trajet demandé.
     *
     * @return stdClass Données du trajet demandé.
     */
    public function fetchTripDetails()
    {
        // Contruction de la requête
        $query = <<<SQL
SELECT UserDisplay.email AS driver_email,
       UserDisplay.full_name AS driver_name,
       Trip.valid AS valid,

       Vehicle.brand AS vehicle_brand,
       Vehicle.model AS vehicle_model,
       Vehicle.color AS vehicle_color,

       CEIL(Trip.price * get_distance(StartAddress.position, EndAddress.position)) AS total_price,
       get_distance(StartAddress.position, EndAddress.position) AS distance,
       get_available_seats_between(StartStop.id, EndStop.id) AS available_seats,

       StartCity.name AS start_city,
       StartStop.meet_time AS start_time,
       EndCity.name AS end_city,
       EndStop.meet_time AS end_time,

       :user IN (
           SELECT participant
             FROM TripParticipants
            WHERE TripParticipants.trip = Trip.id
       ) AS participates

  FROM Trip,
       UserDisplay,
       Vehicle,

       Stop AS StartStop,
       AddressDisplay AS StartAddress,
       City AS StartCity,

       Stop AS EndStop,
       AddressDisplay AS EndAddress,
       City AS EndCity

 WHERE Trip.drive = UserDisplay.email
       AND Trip.use_vehicle = Vehicle.number_plate

       AND Trip.id = StartStop.trip
       AND StartAddress.id = StartStop.be_at
       AND StartAddress.be_in = StartCity.id

       AND Trip.id = EndStop.trip
       AND EndAddress.id = EndStop.be_at
       AND EndAddress.be_in = EndCity.id

       AND Trip.id = :trip
       AND StartStop.id = :start
       AND EndStop.id = :end;
SQL;

        $query_vars = array(
            ':user' => $this->auth->getEmail(),
            ':trip' => $this->trip,
            ':start' => $this->start,
            ':end' => $this->end
        );

        // Préparation et exécution de la requête
        $stmp = $this->db->prepare($query);
        $stmp->execute($query_vars);

        $result = $stmp->fetch(\PDO::FETCH_OBJ);

        // Transformation des dates en instances de DateTime
        if ($result)
        {
            $result->start_time = \DateTime::createFromFormat(
                \Constants::DATETIME_MYSQL_FORMAT,
                $result->start_time
            );

            $result->end_time = \DateTime::createFromFormat(
                \Constants::DATETIME_MYSQL_FORMAT,
                $result->end_time
            );
        }

        return $result;
    }

    /**
     * Récupère les informations sur les arrêts d'un trajet.
     *
     * @return array(stdClass) Informations sur les arrêts du trajet.
    */
    public function fetchStopDetails()
    {
        // Construction de la requête
$query = <<<SQL
  SELECT Stop.id AS stop_id,
         Stop.meet_time AS stop_time,
         AddressDisplay.full_name AS address_name,
         ST_X(AddressDisplay.position) AS position_lng,
         ST_Y(AddressDisplay.position) AS position_lat,
         City.name AS city_name


    FROM Stop,
         AddressDisplay,
         City

   WHERE Stop.be_at = AddressDisplay.id
         AND AddressDisplay.be_in = City.id
         AND Stop.trip = :trip

ORDER BY Stop.meet_time ASC;
SQL;

        $query_vars = array(
            ':trip' => $this->trip
        );

        // Préparation et exécution de la requête
        $stmp = $this->db->prepare($query);
        $stmp->execute($query_vars);

        $results = $stmp->fetchAll(\PDO::FETCH_OBJ);

        // Transformation des dates en instances de DateTime
        foreach ($results as $result)
        {
            $result->stop_time = \DateTime::createFromFormat(
                \Constants::DATETIME_MYSQL_FORMAT,
                $result->stop_time
            );
        }

        return $results;
    }

    public function addPassenger()
    {
        if ($this->auth->hasRole(\User\Role::USER))
        {
            $query = <<<SQL
CALL Trip_add_passenger(:email, :start, :end);
SQL;

            $query_vars = [
                ':email' => $this->auth->getEmail(),
                ':start' => $this->start,
                ':end' => $this->end,
            ];

            $stmp = $this->db->prepare($query);
            $stmp->execute($query_vars);
        }
    }

    public function removePassenger()
    {
        if ($this->auth->hasRole(\User\Role::USER))
        {
            $query = <<<SQL
DELETE StopPickup
  FROM StopPickup, Stop
 WHERE StopPickup.stop = Stop.id
       AND StopPickup.email = :email
       ANd Stop.trip = :trip
SQL;

            $query_vars = [
                ':email' => $this->auth->getEmail(),
                ':trip' => $this->trip,
            ];

            $stmp = $this->db->prepare($query);
            $stmp->execute($query_vars);
        }
    }

    public function getTrip()
    {
        return $this->trip;
    }

    public function setTrip($trip)
    {
        $this->trip = intval($trip);
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setStart($start)
    {
        $this->start = $start;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setEnd($end)
    {
        $this->end = $end;
    }
}

