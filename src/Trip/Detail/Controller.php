<?php
namespace Trip\Detail;

/**
 * Contrôleur des détails des trajets
 */
class Controller
{
    private $model;
    private $form;

    public function __construct(Model $model, \Form\Model $form)
    {
        $this->model = $model;
        $this->form = $form;
    }

    public function show($trip, $start, $end)
    {
        $this->model->setTrip($trip);
        $this->model->setStart($start);
        $this->model->setEnd($end);
    }

    public function add($trip, $start, $end)
    {
        $this->model->setTrip($trip);
        $this->model->setStart($start);
        $this->model->setEnd($end);

        try
        {
            $this->model->addPassenger();
        }
        catch (\PDOException $err)
        {
            $this->form->addError('general', $err->getMessage());
        }
    }

    public function remove($trip, $start, $end)
    {
        $this->model->setTrip($trip);
        $this->model->setStart($start);
        $this->model->setEnd($end);

        try
        {
            $this->model->removePassenger();
        }
        catch (\PDOException $err)
        {
            $this->form->addError('general', $err->getMessage());
        }
    }
}
