<?php
namespace Site\Template;

/**
 * Modèle du squelette du site.
 */
class Model
{
    // Titre de la page
    private $title;

    /**
     * Récupère le titre de la page.
     *
     * @return Titre de la page à afficher.
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Définit le titre de la page.
     *
     * @param title Titre de la page.
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}

