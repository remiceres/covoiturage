<?php
namespace Form;

class ViewHelpers
{
    /**
     * Affiche la liste d’erreurs d’un champ du formulaire.
     *
     * @param form Modèle du formulaire.
     * @param field Champ dont il faut afficher les erreurs.
     */
    public static function renderErrors(Model $form, $field)
    {
        if ($form->isValid($field))
        {
            return;
        }
?>
<ul class="form-errors">
    <?php foreach ($form->getErrors($field) as $error): ?>
    <li><?= $error ?></li>
    <?php endforeach; ?>
</ul>
<?php
    }
}

