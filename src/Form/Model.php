<?php
namespace Form;

/**
 * Modèle pour les formulaires.
 */
class Model
{
    private $_errors = [];

    /**
     * Enregistre une nouvelle erreur sur un champ du formulaire.
     *
     * @param field Nom du champ erroné dans le formulaire.
     * @param message Message décrivant l’erreur.
     */
    public function addError($field, $message)
    {
        if (isset($this->_errors[$field]))
        {
            array_push($this->_errors[$field], $message);
        }
        else
        {
            $this->_errors[$field] = [$message];
        }
    }

    /**
     * Récupère la liste des erreurs liées à un champ du formulaire.
     *
     * @param field Nom du champ à contrôler.
     */
    public function getErrors($field)
    {
        return isset($this->_errors[$field])
            ? $this->_errors[$field]
            : [];
    }

    /**
     * Vérifie si un champ du formulaire est exempt d’erreur ou non.
     *
     * @param field Nom du champ à contrôler.
     * @return Vrai si et seulement si le champ est exempt d’erreur.
     */
    public function isValid($field)
    {
        return empty($this->_errors[$field]);
    }

    /**
     * Vérifie si le formulaire est exempt d’erreur ou non.
     *
     * @return Vrai si et seulement si le formulaire est exempt d’erreur.
     */
    public function isFormValid()
    {
        return empty($this->_errors);
    }
}

