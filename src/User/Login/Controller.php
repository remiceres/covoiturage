<?php
namespace User\Login;

/**
 * Contrôleur de connexion utilisateur.
 */
class Controller
{
    private $router;
    private $auth;
    private $model;
    private $form;

    public function __construct(
        \Router $router, \User\Authenticator $auth,
        Model $model, \Form\Model $form
    )
    {
        $this->router = $router;
        $this->auth = $auth;
        $this->model = $model;
        $this->form = $form;
    }

    public function login($data)
    {
        if (!empty($data['email']))
        {
            $this->model->setEmail($data['email']);

            if (!empty($data['password']))
            {
                if ($this->auth->login($data['email'], $data['password']))
                {
                    $this->router->redirect('home');
                }
                else
                {
                    $this->form->addError(
                        'email',
                        'Nom d’utilisateur ou mot de passe incorrect.'
                    );
                }
            }
            else
            {
                $this->form->addError(
                    'password',
                    'Veuillez saisir un mot de passe pour vous connecter.'
                );
            }
        }
    }
}

