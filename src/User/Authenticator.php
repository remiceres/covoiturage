<?php
namespace User;

/**
 * Gère l’authentification des utilisateurs.
 */
class Authenticator
{
    // Référence à la base de données PDO
    private $db;

    // Identifiant de l’utilisateur connecté, ou NULL
    private $email;

    // Rôle actuel de l’utilisateur
    private $role = Role::NONE;

    // Date d’activation de l’utilisateur
    private $activation_date;

    // Clé de stockage dans le tableau de session
    private const SESSION_KEY = 'Authenticate_user_email';

    /**
     * Crée une instance d’authentificateur d’utilisateur.
     *
     * @param db Référence à la connexion PDO vers la base de données.
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;

        // Ré-authentification de l’utilisateur connecté
        if (isset($_SESSION[self::SESSION_KEY]))
        {
            if (!$this->loginWithoutPassword($_SESSION[self::SESSION_KEY]))
            {
                // En cas d’échec, oubli des informations invalides
                unset($_SESSION[self::SESSION_KEY]);
            }
        }
    }

    /**
     * Récupère l’identifiant de l’utilisateur authentifié.
     *
     * @return Email de l’utilisateur authentifié, ou NULL
     * si aucun utilisateur n’est authentifié.
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Récupère le rôle de l’utilisateur.
     *
     * @return Rôle actuel de l’utilisateur.
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Vérifie que l’utilisateur dispose au moins d’un rôle.
     *
     * @param role Rôle minimal à vérifier.
     * @return Vrai si et seulement si l’utilisateur dispose de ce rôle.
     */
    public function hasRole($role)
    {
        return $this->role >= $role;
    }

    /**
     * Récupère la date d’activation de l’utilisateur authentifié.
     *
     * @return Date à partir de laquelle l’utilisateur est autorisé
     * à se connecter, ou NULL si aucun utilisateur n’est authentifié.
     */
    public function getActivationDate()
    {
        return $this->activation_date;
    }

    /**
     * Authentifie un utilisateur avec son mot de passe.
     *
     * @param email Identifiant de l’utilisateur à authentifier.
     * @param password Mot de passe de l’utilisateur.
     * @return Vrai si et seulement si l’authentification s’est terminée
     * avec succès.
     */
    public function login($email, $password)
    {
        $query = <<<SQL
SELECT email, password, role, activation_date
  FROM User
 WHERE email = :email;
SQL;

        $stmp = $this->db->prepare($query);
        $stmp->execute(array(
            ':email' => $email
        ));

        $infos = $stmp->fetch(\PDO::FETCH_OBJ);

        if ($infos)
        {
            if (password_verify($password, $infos->password))
            {
                $this->hydrate($infos);
                $_SESSION[self::SESSION_KEY] = $email;

                return true;
            }
        }

        return false;
    }

    /**
     * Authentifie un utilisateur sans son mot de passe
     * (à utiliser avec prudence !)
     *
     * @param email Identifiant de l’utilisateur à authentifier.
     * @return Vrai si et seulement si l’authentification s’est terminée
     * avec succès.
     */
    public function loginWithoutPassword($email)
    {
        $query = <<<SQL
SELECT email, role, activation_date
  FROM User
 WHERE email = :email;
SQL;

        $stmp = $this->db->prepare($query);
        $stmp->execute(array(
            ':email' => $email
        ));

        $infos = $stmp->fetch(\PDO::FETCH_OBJ);

        if ($infos)
        {
            $this->hydrate($infos);
            $_SESSION[self::SESSION_KEY] = $email;

            return true;
        }

        return false;
    }

    /**
     * Transforme les données brutes de la base de données
     * et les stocke dans l’instance actuelle.
     *
     * @param infos Informations brutes à stocker.
     */
    private function hydrate(\stdClass $infos)
    {
        $this->email = $infos->email;
        $this->role = $infos->role === 'admin' ? Role::ADMIN : Role::USER;
        $this->activation_date = \DateTime::createFromFormat(
            \Constants::DATETIME_MYSQL_FORMAT,
            $infos->activation_date
        );
    }

    /**
     * Déconnecte l’utilisateur actuel.
     */
    public function logout()
    {
        $this->email = NULL;
        $this->role = Role::NONE;
        $this->activation_date = NULL;

        unset($_SESSION[self::SESSION_KEY]);
    }
}

