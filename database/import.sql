/**
 * Fichier import.sql
 * Contient tous les ordres d'importation
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */

source functions.sql
source structure.sql
source triggers.sql
source data.sql
source user.sql
