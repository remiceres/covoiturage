/**
 * Fichier structure.sql
 * Contient les ordres de création des relations et vues
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */

CREATE DATABASE IF NOT EXISTS covoiturage
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE covoiturage;

-- Table City

CREATE TABLE City(
    id NUMERIC,
    name VARCHAR(256) NOT NULL,
    position POINT NOT NULL,
    importance NUMERIC(6, 5) NOT NULL,

    CONSTRAINT pk_city PRIMARY KEY(id),
    INDEX ind_city_name (name, importance)
);

-- Table PostalCode

CREATE TABLE PostalCode(
    postal_code VARCHAR(8),
    city NUMERIC,

    CONSTRAINT pk_postal_code PRIMARY KEY(postal_code, city),
    CONSTRAINT fk_postal_code_city FOREIGN KEY(city) REFERENCES City(id)
);

-- Table Address

CREATE TABLE Address(
    id NUMERIC,
    num VARCHAR(6),
    name VARCHAR(256) NOT NULL,
    position POINT NOT NULL,
    importance NUMERIC(6, 5) NOT NULL,
    be_in NUMERIC NOT NULL,

    CONSTRAINT pk_address PRIMARY KEY(id),
    CONSTRAINT fk_address_be_in FOREIGN KEY(be_in) REFERENCES City(id),
    INDEX ind_address_name (num, name, importance)
);

-- Vue AddressDisplay
--
-- Contient la version pré-formattée des adresses dérivée des
-- différents attributs de Address et City

CREATE VIEW AddressDisplay AS
    SELECT Address.id AS id,
           CONCAT(Address.num, ' ', Address.name, ', ', City.name) AS full_name,
           Address.position AS position,
           Address.importance AS importance,
           Address.be_in AS be_in
      FROM Address, City
     WHERE Address.be_in = City.id;

-- Table Vehicule

CREATE TABLE Vehicle(
    number_plate VARCHAR(16),
    brand VARCHAR(128) NOT NULL,
    model VARCHAR(128),
    color VARCHAR(64) NOT NULL,

    CONSTRAINT pk_vehicle PRIMARY KEY(number_plate)
);

-- Table User

CREATE TABLE User(
    email VARCHAR(256),
    password CHAR(60) NOT NULL,
    role VARCHAR(5) NOT NULL,
    last_name VARCHAR(64),
    first_name VARCHAR(64),
    birthdate DATE,
    tel_indic VARCHAR(3) NOT NULL,
    tel_number VARCHAR(16) NOT NULL,
    activation_date DATE NOT NULL,
    reside_at NUMERIC NOT NULL,

    CONSTRAINT pk_user PRIMARY KEY(email),
    CONSTRAINT dom_user_email CHECK(email LIKE '%@%.%'),
    CONSTRAINT dom_user_role CHECK(role IN ('admin', 'user')),
    CONSTRAINT dom_user_birthdate CHECK(DATEDIFF(CURRENT_DATE(), birthdate) > 0),
    CONSTRAINT dom_user_tel_indic CHECK(tel_indic REGEXP '[0-9]{1,3}'),
    CONSTRAINT dom_user_tel_number CHECK(tel_number REGEXP '[0-9]{1,}'),
    CONSTRAINT fk_user_reside_at FOREIGN KEY(reside_at) REFERENCES Address(id)
);

-- Table Trip

CREATE TABLE Trip(
    id NUMERIC,
    price NUMERIC(6) NOT NULL,
    seats NUMERIC(3) NOT NULL,
    valid BOOLEAN NOT NULL,
    drive VARCHAR(256) NOT NULL,
    use_vehicle VARCHAR(16) NOT NULL,

    CONSTRAINT pk_trip PRIMARY KEY(id),
    CONSTRAINT dom_trip_price CHECK(price > 0),
    CONSTRAINT dom_trip_seats CHECK(seats > 0),
    CONSTRAINT fk_trip_drive FOREIGN KEY(drive) REFERENCES User(email),
    CONSTRAINT fk_trip_use_vehicle FOREIGN KEY(use_vehicle)
        REFERENCES Vehicle(number_plate)
);

-- Table Stop

CREATE TABLE Stop(
    id NUMERIC,
    meet_time DATETIME NOT NULL,
    trip NUMERIC NOT NULL,
    be_at NUMERIC NOT NULL,

    CONSTRAINT pk_stop PRIMARY KEY(id),
    CONSTRAINT fk_stop_trip FOREIGN KEY(trip) REFERENCES Trip(id),
    CONSTRAINT fk_stop_be_at FOREIGN KEY(be_at) REFERENCES Address(id)
);

-- Table TripReview

CREATE TABLE TripReview(
    from_user VARCHAR(256) NOT NULL,
    about_user VARCHAR(256) NOT NULL,
    trip NUMERIC NOT NULL,
    rating NUMERIC(1) NOT NULL,
    comment VARCHAR(512),

    CONSTRAINT pk_trip_review PRIMARY KEY(from_user, about_user, trip),
    CONSTRAINT dom_trip_review_rating CHECK (rating BETWEEN 0 AND 5),
    CONSTRAINT fk_trip_review_from_user FOREIGN KEY(from_user)
        REFERENCES User(email),
    CONSTRAINT fk_trip_review_about_user FOREIGN KEY(about_user)
        REFERENCES User(email),
    CONSTRAINT fk_trip_review_trip FOREIGN KEY(trip) REFERENCES Trip(id)
);

-- Vue UserDisplay
--
-- Donne les informations sur les utilisateurs du site accompagnées
-- de données calculées telles que la note moyenne ou le nom complet

CREATE VIEW UserDisplay AS
  SELECT User.*, AVG(TripReview.rating) AS rating,
         CONCAT(User.first_name, ' ', User.last_name) AS full_name
    FROM User, TripReview
   WHERE User.email = TripReview.about_user
GROUP BY User.email
UNION
  SELECT User.*, NULL AS rating,
         CONCAT(User.first_name, ' ', User.last_name) AS full_name
    FROM User
   WHERE User.email NOT IN (
             SELECT TripReview.about_user
               FROM TripReview
         );

-- Table CityLinkData

CREATE TABLE CityLinkData(
    from_city NUMERIC,
    to_city NUMERIC,
    max_price NUMERIC(6) NOT NULL,

    CONSTRAINT pk_city_link PRIMARY KEY(from_city, to_city),
    CONSTRAINT fk_city_link_from_city FOREIGN KEY(from_city)
        REFERENCES City(id),
    CONSTRAINT fk_city_link_to_city FOREIGN KEY(to_city)
        REFERENCES City(id),
    CONSTRAINT dom_city_link_max_price CHECK(max_price > 0)
);

-- Vue CityLink
--
-- Affiche les données de CityLinkData adjointes de données calculées
-- telles que la distance et le temps moyen de parcours

CREATE VIEW CityLink AS
    SELECT from_city, to_city, max_price,
           get_distance(C_start.position, C_end.position) AS distance,
           get_time_estimate(C_start.position, C_end.position) AS mean_time
      FROM CityLinkData, City C_start, City C_end
     WHERE CityLinkData.from_city = C_start.id
           AND CityLinkData.to_city = C_end.id;

-- Table VehicleOwn

CREATE TABLE VehicleOwn(
    vehicle VARCHAR(16),
    user VARCHAR(256),

    CONSTRAINT pk_vehicle_own PRIMARY KEY(vehicle, user),
    CONSTRAINT fk_vehicle_own_vehicle FOREIGN KEY(vehicle)
        REFERENCES Vehicle(number_plate),
    CONSTRAINT fk_vehicle_own_user FOREIGN KEY(user) REFERENCES User(email)
);

-- Table StopDeposit

CREATE TABLE StopDeposit(
    email VARCHAR(256),
    stop NUMERIC,

    CONSTRAINT pk_stop_deposit PRIMARY KEY(email, stop),
    CONSTRAINT fk_stop_deposit_email FOREIGN KEY(email) REFERENCES User(email),
    CONSTRAINT fk_stop_deposit_stop FOREIGN KEY(stop) REFERENCES Stop(id)
);

-- Table StopPickup

CREATE TABLE StopPickup(
    email VARCHAR(256),
    stop NUMERIC,

    CONSTRAINT pk_stop_pickup PRIMARY KEY(email, stop),
    CONSTRAINT fk_stop_pickup_email FOREIGN KEY(email) REFERENCES User(email),
    CONSTRAINT fk_stop_pickup_stop FOREIGN KEY(stop) REFERENCES Stop(id)
);

-- Vue TripParticipants
--
-- Liste des participants pour chaque trajet

CREATE VIEW TripParticipants AS
SELECT Trip.id AS trip, Trip.drive AS participant, 'driver' AS type
  FROM Trip
UNION
SELECT Trip.id AS trip, StopPickup.email AS paticipant, 'participant' AS type
  FROM Trip, Stop, StopPickup
 WHERE Trip.id = Stop.trip
       AND Stop.id = StopPickup.stop;
