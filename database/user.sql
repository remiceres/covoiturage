/**
 * Fichier user.sql
 * Créé l'utilisateur pour PHP disposant des permissions minimales nécessaires
 * à son fonctionnement
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */

CREATE DATABASE IF NOT EXISTS covoiturage
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE covoiturage;

DROP USER IF EXISTS covoiturage;

CREATE USER covoiturage
IDENTIFIED BY 'covoiturage';

GRANT SELECT ON City TO covoiturage;
GRANT SELECT ON PostalCode TO covoiturage;
GRANT SELECT ON Address TO covoiturage;
GRANT SELECT ON AddressDisplay TO covoiturage;
GRANT SELECT, INSERT, UPDATE, DELETE ON Vehicle TO covoiturage;
GRANT SELECT, INSERT, UPDATE, DELETE ON User TO covoiturage;
GRANT SELECT ON UserDisplay TO covoiturage;
GRANT SELECT, INSERT, UPDATE ON Trip TO covoiturage;
GRANT SELECT ON TripParticipants TO covoiturage;
GRANT SELECT, INSERT, UPDATE, DELETE ON Stop TO covoiturage;
GRANT SELECT, INSERT, UPDATE, DELETE ON TripReview TO covoiturage;
GRANT INSERT, UPDATE, DELETE ON CityLinkData TO covoiturage;
GRANT SELECT ON CityLink TO covoiturage;
GRANT SELECT, DELETE ON StopDeposit TO covoiturage;
GRANT SELECT, DELETE ON StopPickup TO covoiturage;
GRANT SELECT, INSERT, UPDATE, DELETE ON VehicleOwn TO covoiturage;
GRANT EXECUTE ON PROCEDURE Trip_add_passenger TO covoiturage;
GRANT EXECUTE ON FUNCTION get_occupied_seats TO covoiturage;
GRANT EXECUTE ON FUNCTIOn get_available_seats_between TO covoiturage;
GRANT EXECUTE ON FUNCTION get_distance TO covoiturage;
GRANT EXECUTE ON FUNCTION get_time_estimate TO covoiturage;
