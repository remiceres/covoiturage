/**
 * Fichier functions.sql
 * Contient les définitions de fonctions
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */

CREATE DATABASE IF NOT EXISTS covoiturage
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE covoiturage;
DELIMITER $$

-- Fonction get_occupied_seats
--
-- Détermine le nombre de sièges occupés dans un trajet
-- à un arrêt donné de ce trajet
--
-- Paramètre : stop_id (identifiant de l’arrêt)
-- Résultat : nombre de sièges occupés à la fin de cet arrêt

CREATE FUNCTION get_occupied_seats(stop_id NUMERIC)
RETURNS NUMERIC
BEGIN
    DECLARE trip_id NUMERIC;
    DECLARE stop_time DATETIME;

    DECLARE pickups NUMERIC(10) DEFAULT 0;
    DECLARE deposits NUMERIC(10) DEFAULT 0;

    SELECT Stop.trip, Stop.meet_time
      INTO trip_id, stop_time
      FROM Stop
     WHERE Stop.id = stop_id;

    SELECT COUNT(*) INTO pickups
      FROM StopPickup, Stop
     WHERE StopPickup.stop = Stop.id
           AND Stop.trip = trip_id
           AND Stop.meet_time <= stop_time;

    SELECT COUNT(*) INTO deposits
      FROM StopDeposit, Stop
     WHERE StopDeposit.stop = Stop.id
           AND Stop.trip = trip_id
           AND Stop.meet_time <= stop_time;

    RETURN pickups - deposits;
END$$

-- Fonction get_available_seats_between
--
-- Détermine le nombre de places disponibles dans un trajet entre
-- deux arrêts d'un trajet global
--
-- Paramètres :
--     - start_stop (arrêt de départ)
--     - end_stop (arrêt de fin)
--
-- Résultat : nombre de sièges disponibles pour ce trajet

CREATE FUNCTION get_available_seats_between(
    start_stop NUMERIC,
    end_stop NUMERIC
)
RETURNS NUMERIC
BEGIN
    DECLARE result NUMERIC;

    DECLARE start_trip NUMERIC;
    DECLARE start_time DATETIME;

    DECLARE end_trip NUMERIC;
    DECLARE end_time DATETIME;

    DECLARE seats NUMERIC;

    SELECT Stop.trip, Stop.meet_time
      INTO start_trip, start_time
      FROM Stop
     WHERE Stop.id = start_stop;

    SELECT Stop.trip, Stop.meet_time
      INTO end_trip, end_time
      FROM Stop
     WHERE Stop.id = end_stop;

    IF start_trip <> end_trip THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'L''arrêt de départ et celui d''arrivée n''appartiennent pas au même voyage';
    END IF;

    SELECT Trip.seats INTO seats
      FROM Trip
     WHERE Trip.id = start_trip;

    SELECT seats - MAX(get_occupied_seats(Stop.id)) INTO result
      FROM Stop
     WHERE Stop.trip = start_trip
           AND Stop.meet_time >= start_time
           AND Stop.meet_time < end_time;

    RETURN result;
END$$

-- Fonction get_distance
--
-- Estime la distance en kilomètres entre deux points terrestres
--
-- Paramètres :
--     - start (point de départ de la mesure)
--     - end (point d'arrivée de la mesure)
--
-- Résultat : distance estimée en kilomètres entre les deux points

CREATE FUNCTION get_distance(start POINT, end POINT)
RETURNS NUMERIC(10, 5)
BEGIN
    RETURN 6371 * ACOS(
        COS(RADIANS(ST_Y(start)))
        * COS(RADIANS(ST_Y(end)))
        * COS(RADIANS(ST_X(end)) - RADIANS(ST_X(start)))
        + SIN(RADIANS(ST_Y(start)))
        * SIN(RADIANS(ST_Y(end)))
    );
END$$

-- Fonction get_time_estimate
--
-- Estime le temps moyen en heures nécessaire pour aller d'un point
-- à un autre en voiture
--
-- Paramètres :
--     - start (point de départ de la mesure)
--     - end (point d'arrivée de la mesure)
--
-- Résultat : temps estimé en heures entre les deux points

CREATE FUNCTION get_time_estimate(start POINT, end POINT)
RETURNS NUMERIC(10, 5)
BEGIN
    RETURN get_distance(start, end) / 80;
END$$

DELIMITER ;
